var gulp = require('gulp');
var gutil = require('gulp-util');


/* *************
  Config
************* */

var globalData = {
    base: require('./src/data/base.json')
};


/* *************
  CSS
************* */

var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var scss = require('postcss-scss');
var autoprefixer = require('autoprefixer');
var postcssProcessors = [
    autoprefixer( { browsers: ['last 2 versions', 'ie > 10'] } )
]

gulp.task('sassInline', function(callback) {
    return gulp.src('src/sass/inline.scss')
        .pipe(
           postcss(postcssProcessors, {syntax: scss})
        )
        .pipe(
            sass({ outputStyle: 'expanded' })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/css/'));
});

gulp.task('sassEmbedded', function(callback) {
    return gulp.src('src/sass/embedded.scss')
        .pipe(
           postcss(postcssProcessors, {syntax: scss})
        )
        .pipe(
            sass({ outputStyle: 'compressed' })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/css/')); 
});

gulp.task('sassReset', function(callback) {
  return gulp.src('src/sass/reset.scss')
      .pipe(
         postcss(postcssProcessors, {syntax: scss})
      )
      .pipe(
          sass({ outputStyle: 'compressed' })
          .on('error', gutil.log)
      )
      .pipe(gulp.dest('build/css/')); 
});

gulp.task('sassMCReset', function(callback) {
  return gulp.src('src/sass/mc-reset.scss')
      .pipe(
         postcss(postcssProcessors, {syntax: scss})
      )
      .pipe(
          sass({ outputStyle: 'compressed' })
          .on('error', gutil.log)
      )
      .pipe(gulp.dest('build/css/')); 
});

var inlineCss = require('gulp-inline-css');

gulp.task('inlinecss', ['sassInline', 'nunjucks'], function() {
    return gulp.src('build/*.html')
        .pipe(
            inlineCss({
                applyStyleTags: false,
                removeStyleTags: false
            })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/'))

});



/* *************
  TEMPLATING
************* */

var nunjucksRender = require('gulp-nunjucks-render');
var data = require('gulp-data');

gulp.task('nunjucks', ['sassEmbedded', 'sassReset', 'sassMCReset'], function() {
    return gulp.src('src/emails/*.nunjucks')
        .pipe(
            data(function() {
                return globalData;
            })
            .on('error', gutil.log)
        )
        .pipe(
            nunjucksRender({
                path: ['src/templates/', 'build/css/']
            })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/'));
});

/* *************
  REMOVE UNUSED CSS
************* */

var tap = require("gulp-tap");
var { comb } = require("email-comb");
var util = require("gulp-util");
var using = require('gulp-using');
var removeHTMLComments = false;
var whitelist = [
    ".External*",
    ".ReadMsgBody",
    ".yshortcuts",
    ".Mso*",
    "#outlook",
    ".module*"
  ];

 
gulp.task('removeUnusedCSS', ['nunjucks', 'inlinecss'], () => {
  return gulp.src("build/*.html").pipe(using()).pipe(
    tap(file => {
        var cleanedHtmlResult = comb(file.contents.toString(), {
          whitelist, removeHTMLComments
        });
        util.log(
            `Removed`,
            util.colors.yellow (`${cleanedHtmlResult.deletedFromHead.length}`), 
            `styles from Head and`,
            util.colors.yellow (`${cleanedHtmlResult.deletedFromBody.length}`),
            `styles from Body`
        
        );
        file.contents = Buffer.from(cleanedHtmlResult.result);
      })
    )
    .pipe(gulp.dest('build/'));
  });

/* *************
  REMOVE EMPTY LINES
************* */


  var gulp = require('gulp');
  var removeEmptyLines = require('gulp-remove-empty-lines');

  gulp.task('removeEmptyLines', ['removeUnusedCSS'], function () {
    gulp.src('build/*.html')
    .pipe(removeEmptyLines({
      removeComments: false
    }))
    .pipe(gulp.dest('build/'))
    .pipe(connect.reload());
  });


/* *************
    ZIP
************* */

var zip = require('gulp-zip');

gulp.task('zip', function () {
    return gulp.src('build/**')
        .pipe(zip('build.zip'))
        .pipe(gulp.dest('./'));
});


/* *************
	SERVER
************* */

var connect = require('gulp-connect');

gulp.task('connect', function() {
    connect.server({
        port: 8000,
        root: 'build', // Serve from build directory instead,
        livereload:true
    });
});



/* *************
    WATCH
************* */

var filesToWatch = [
    'src/sass/**/*.scss',
    'src/emails/*.nunjucks',
    'src/templates/**/*.nunjucks',
    'src/emails/*.njk',
    'src/templates/**/*.njk',
    'src/data/*.json'
]

gulp.task('watch', function() {
    gulp.watch(filesToWatch,['nunjucks', 'inlinecss', 'removeEmptyLines']); 
});


gulp.task('watch-old', function() {
    gulp.watch(filesToWatch,['nunjucks', 'inlinecss']); 
});

gulp.task('old', ['connect', 'nunjucks', 'inlinecss', 'watch-old']);

/* *************
    DEFAULT
************* */

gulp.task('default', ['connect', 'nunjucks', 'inlinecss', 'removeEmptyLines', 'watch']);

