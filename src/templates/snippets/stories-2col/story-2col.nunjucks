{% set image_width = image_width if image_width else "280" %} {# Set default image width #}
{% set image_height = image_height if image_height else "200" %} {# Set default image height #}
{% set background_class = "story-2col-bg" if (background == true) else "" %} {# If true add background class #} 

{% set valign = valign if valign else "top" %} {# Set vertical alignment #} 
{% set align = align if align else "left" %} {# Set desktop alignment #} 
{% set mobile_align = mobile_align if mobile_align else "center" %} {# Set mobile alignment #} 

{% set bt_classes = "button button-"  + align + " button-mobile-" + mobile_align %} {# Classes added to button wrapper #}  

{% if background_color == null %}
    {% set background_color = "#ffffff" %}
{% endif %}

{% if (base.platform == 'me') %}
<!-- START .story-2col -->
{% endif %}
<table role="presentation" class="imp content-outer story-2col {{custom_class}} {{background_class}}" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
        {% if (show_side !== false) %}
            <td class="imp side">&nbsp;</td>
        {% endif %}
        <td class="imp content-inner" valign="{{valign}}" align="center">
            <table role="presentation" class="imp section" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="imp col-1-2 block" align="center" valign="{{valign}}">
                    {% if (background_image_0) %}
                        {% set background_image = (background_image_0 if background_image_0 else "" ) %}
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td background="{{background_image}}" bgcolor="{{background_color}}" valign="{{valign}}">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000">
                                <v:fill type="frame" src="{{background_image}}" color="{{background_color}}" />
                                <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                            <![endif]-->
                            <div>
                        {% endif %}
                                <table role="presentation" class="imp section" align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            {% set story_image = (story_image_0 if story_image_0 else "" ) %}
                                            {% include base.path + "story-image.njk" %}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="{{align}}" class="imp section-content">
                                        {% if (custom_content_0) %}
                                            {{custom_content_0|safe}}
                                        {% else %}
                                            <table class="imp section" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="imp story-title-spacer before">&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="imp desktop-text-{{align}} mobile-text-{{mobile_align}}">
                                                        {% if (title_image_0) %}
                                                            {% set title_image = (title_image_0 if title_image_0 else "" ) %}
                                                            {% include base.path + "title-image.njk" %}
                                                        {% else %}
                                                            {% set story_title = (story_title_0 if story_title_0 else "" ) %}
                                                            <h3 class="imp story-title">{% include base.path + "story-title.njk" %}</h3>
                                                        {% endif %}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="imp story-title-spacer after">&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="imp story-intro desktop-text-{{align}} mobile-text-{{mobile_align}}">
                                                        {% set story_intro = (story_intro_0 if story_intro_0 else "" ) %}
                                                        {% include base.path + "story-intro.njk" %}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="imp story-intro-spacer after">&nbsp;</div>
                                                    </td>
                                                </tr>
                                                {% if (show_button !== false) %}
                                                    <tr>
                                                        <td>
                                                            <table role="presentation" class="imp section section-button" align="{{align}}" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="imp col-full block" valign="{{valign}}" align="{{align}}">
                                                                        {% set button_title = (button_title_0 if button_title_0 else "" ) %}
                                                                        {% include base.path + "story-button.njk" %}
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="imp section-button-spacer after">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                {% endif %}
                                            </table>
                                            {% endif %}
                                        </td>
                                    </tr>
                                </table>
                        {% if (background_image_0) %}
                                    </div>
                                    <!--[if gte mso 9]>
                                        </v:textbox>
                                    </v:rect>
                                    <![endif]-->
                                    </td>
                                </tr>
                            </table>
                        {% endif %}  

                    </td>
                    <td class="imp gap block">&nbsp;</td>
                    <td class="imp col-1-2 block" align="center" valign="{{valign}}">
                    {% if (background_image_1) %}
                        {% set background_image = (background_image_1 if background_image_1 else "" ) %}
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td background="{{background_image}}" bgcolor="{{background_color}}" valign="{{valign}}">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000">
                                <v:fill type="frame" src="{{background_image}}" color="{{background_color}}" />
                                <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                            <![endif]-->
                            <div>
                        {% endif %}
                                <table role="presentation" class="imp section" align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            {% set story_image = (story_image_1 if story_image_1 else "" ) %}
                                            {% include base.path + "story-image.njk" %}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="{{align}}" class="imp section-content">
                                            {% if (custom_content_1) %}
                                                {{custom_content_1|safe}}
                                            {% else %}  
                                                <table class="imp section" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="imp story-title-spacer before">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="imp desktop-text-{{align}} mobile-text-{{mobile_align}}">
                                                            {% if (title_image_1) %}
                                                                {% set title_image = (title_image_1 if title_image_1 else "" ) %}
                                                                {% include base.path + "title-image.njk" %}
                                                            {% else %}
                                                                {% set story_title = (story_title_1 if story_title_1 else "" ) %}
                                                                <h3 class="imp story-title">{% include base.path + "story-title.njk" %}</h3>
                                                            {% endif %}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="imp story-title-spacer after">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="imp story-intro desktop-text-{{align}} mobile-text-{{mobile_align}}">
                                                            {% set story_intro = (story_intro_1 if story_intro_1 else "" ) %}
                                                            {% include base.path + "story-intro.njk" %}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="imp story-intro-spacer after">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    {% if (show_button !== false) %}
                                                        <tr>
                                                            <td>
                                                                <table role="presentation" class="imp section section-button" align="{{align}}" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="imp col-full block" valign="{{valign}}" align="{{align}}">
                                                                            {% set button_title = (button_title_1 if button_title_1 else "" ) %}
                                                                            {% include base.path + "story-button.njk" %}
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="imp section-button-spacer after">&nbsp;</div>
                                                            </td>
                                                        </tr>
                                                    {% endif %}
                                                </table>
                                            {% endif %}
                                        </td>
                                    </tr>
                                </table>
                        {% if (background_image_1) %}
                                    </div>
                                    <!--[if gte mso 9]>
                                        </v:textbox>
                                    </v:rect>
                                    <![endif]-->
                                    </td>
                                </tr>
                            </table>
                        {% endif %}  
                    </td>
                </tr>
            </table>
            <div class="imp spacer">&nbsp;</div>
        </td>
        {% if (show_side !== false) %}
            <td class="imp side">&nbsp;</td>
        {% endif %}
    </tr>
</table>
{% if (base.platform == 'me') %}
<!-- END .story-2col -->
{% endif %}